import { Component, OnInit } from '@angular/core';
import { CategoriaService } from 'src/app/services/categoria/categoria.service';
import { ModalController, AlertController } from '@ionic/angular';
import { CrearcategoriaPage } from '../modals/crearcategoria/crearcategoria.page';
import { MensajesService } from 'src/app/services/wait/mensajes.service';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.page.html',
  styleUrls: ['./categorias.page.scss'],
})
export class CategoriasPage implements OnInit {

  categorias = [];

  constructor(
    private categoriaService: CategoriaService,
    private modalctrl: ModalController,
    private alertController: AlertController,
    private msgsservice: MensajesService) { }

  ngOnInit() {
  }

  ngAfterContentInit(): void {
    this.cargarCategorias();
  }
  /* 
    async crear(categoria) {
      
      let modal;
      if (!categoria) {
        modal = await this.modalctrl.create({
          component: CrearcategoriaPage,
          componentProps:{tipo:"Crear Categoria"}
        });
        await modal.present();
      }
      else{
        modal = await this.modalctrl.create({
          component: CrearcategoriaPage,
          componentProps: {categoria,tipo:"Editar Categoria"}
        });
        await modal.present();
      }
    }
   */


  async confirmareliminacion(id: string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirmar',
      message: '<strong>¿Está seguro de eliminar este elemento?</strong>',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            this.eliminar(id);
          }
        }
      ]
    });

    await alert.present();
  }

  async alertaEditar(categoria) {
    let alert;
    if (categoria) {

      alert = await this.alertController.create({
        header: 'Editar',
        inputs: [
          {
            name: 'nombre',
            id: 'nombre',
            type: 'text',
            placeholder: 'Nombre',
            value: categoria.titulo
          },
          // multiline input.
          {
            name: 'descripcion',
            id: 'descripcion',
            type: 'textarea',
            placeholder: 'Descripción',
            value: categoria.descripcion
          }
        ],
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('Confirm Cancel');
            }
          }, {
            text: 'Aceptar',
            handler: (alertData) => { //takes the data 
              console.log(alertData.nombre);
              this.editar(categoria._id, alertData.nombre, alertData.descripcion);
            }
          }
        ]
      });
      await alert.present();
    }
    else {
      alert = await this.alertController.create({
        header: 'Crear Categoria',
        inputs: [
          {
            name: 'nombre',
            id: 'nombre',
            type: 'text',
            placeholder: 'Nombre',
          },
          // multiline input.
          {
            name: 'descripcion',
            id: 'descripcion',
            type: 'textarea',
            placeholder: 'Descripción',
          }
        ],
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('Confirm Cancel');
            }
          }, {
            text: 'Aceptar',
            handler: (alertData) => { //takes the data 
              console.log(alertData.nombre);
              this.crear(alertData.nombre, alertData.descripcion);
            }
          }
        ]
      });
      await alert.present();
    }
  }

  private cargarCategorias() {
    this.categoriaService.getCategorias().subscribe(res => {
      this.categorias = res.categorias;
      console.log(res.categorias)
    })
  }

  private editar(id: string, titulo: string, descripcion: string) {
    this.msgsservice.presentLoading('Procesando solicitud');
    this.categoriaService.editarCategoria(id, titulo, descripcion).subscribe(res => {
      this.msgsservice.dismissLoading();
      this.msgsservice.showAlert('Correcto','Se edito correctamente');
      this.cargarCategorias();
      console.log(res)
    }, err => {
      this.msgsservice.dismissLoading();
      this.msgsservice.showAlert('Error','Ocurrio un error en el proceso');
    })
  }

  private crear(titulo:string,descripcion){
    this.msgsservice.presentLoading("Procesando solicitud");
    this.categoriaService.guardarcategoria(titulo,descripcion).subscribe(res=>{
      this.msgsservice.dismissLoading();
      this.msgsservice.showAlert('Correcto','Se creo correctamente');
      this.cargarCategorias();
    },err=>{
      this.msgsservice.dismissLoading();
      this.msgsservice.showAlert('Error','Ocurrio un error en el proceso');
    });
  }  
  
  private eliminar(id: string) {
    this.categoriaService.eliminarCategoria(id).subscribe(res=>{
      this.cargarCategorias();
      this.msgsservice.showAlert('Correcto','Se elimino correctamente');
    },err=>{
      this.msgsservice.showAlert('Error','Ocurrio un error en el proceso');
    });
  }
}
