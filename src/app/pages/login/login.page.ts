import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { MensajesService } from 'src/app/services/wait/mensajes.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  user = {
    email: '',
    password: ''
  };
  constructor(private _authservice:AuthService,private route:Router,private msgs:MensajesService) {
    if(this._authservice.usuarioData){
      this.route.navigate(['/home'])
    }
   }

  ngOnInit() {

  }

  iniciar(){
    this.msgs.presentLoading("Iniciando Sesión");
     this._authservice.login(this.user).subscribe((res)=>{
       if(res.jwt){
         this.route.navigate(['/home'])
       }
      this.msgs.dismissLoading();
    }, err=>{
      this.msgs.dismissLoading();
      this.msgs.showAlert("Error","Credenciales incorrectas");
    })
  }
}
