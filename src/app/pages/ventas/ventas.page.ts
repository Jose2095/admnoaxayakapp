import { Component, OnInit } from '@angular/core';
import { VentasService } from 'src/app/services/ventas/ventas.service';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.page.html',
  styleUrls: ['./ventas.page.scss'],
})
export class VentasPage implements OnInit {
  ventas = [];
  constructor(private ventaservice: VentasService) { }
  
  ngOnInit() {
  }

  ngAfterContentInit(): void {
    this.ventaservice.listadoVenta().subscribe(res=>{
      this.ventas = res.ventas;
      console.log(res);
    })
  }
}
