import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-crearcategoria',
  templateUrl: './crearcategoria.page.html',
  styleUrls: ['./crearcategoria.page.scss'],
})
export class CrearcategoriaPage implements OnInit {

  @Input() tipo;
  @Input() categoria;
  constructor(private modalctrl: ModalController) { }

  ngOnInit() {
  }

  cerrar() {
    this.modalctrl.dismiss();
    console.log(this.categoria);
    
  }

}
