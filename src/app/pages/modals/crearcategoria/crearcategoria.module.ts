import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CrearcategoriaPageRoutingModule } from './crearcategoria-routing.module';

import { CrearcategoriaPage } from './crearcategoria.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CrearcategoriaPageRoutingModule
  ],
  declarations: [CrearcategoriaPage]
})
export class CrearcategoriaPageModule {}
