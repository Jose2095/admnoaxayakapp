import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrearcategoriaPage } from './crearcategoria.page';

const routes: Routes = [
  {
    path: '',
    component: CrearcategoriaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrearcategoriaPageRoutingModule {}
