import { StringMap } from '@angular/compiler/src/compiler_facade_interface';

export interface Usuario {
    nombre: string,
    token: string
}
