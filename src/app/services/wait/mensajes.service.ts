import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class MensajesService {

  constructor(private loadingController: LoadingController,private alertController:AlertController) { }
  private loading ;
  
//----------------------------------------------------------------
  //open loading alert
  async presentLoading(mensaje:string){
  this.loading = await this.loadingController.create({
      message: mensaje,
      backdropDismiss:false,
      keyboardClose:false,
      spinner:'lines'
    });
    await this.loading.present();
return;
  }

  //close loading message
  dismissLoading(){
    this.loading.dismiss();
    return;
  }

  //-------------------------------------------------------------

  //show alert dialog
  async showAlert(titulo:string,mensaje:string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: titulo,
      message: mensaje,
      buttons: ['Aceptar']
    });

    await alert.present();
  }
}
