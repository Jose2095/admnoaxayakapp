import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable, BehaviorSubject } from 'rxjs';
import { Usuario } from '../models/usuario';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private URL = 'https://oaxayak.mx/api/user/login';
  private userSubject: BehaviorSubject<Usuario>;

  public get usuarioData(): Usuario {
    return this.userSubject.value;
  }


  constructor(private _http: HttpClient,private router:Router) {
    try {
      this.userSubject = new BehaviorSubject(
        JSON.parse(localStorage.getItem('usuario'))
      )
      
    } catch (err) { }
  }

  login(usuario): Observable<any> {
    
    return this._http.post<any>(this.URL, { email: usuario.email, password: usuario.password })
      .pipe(map(res => {
        if (res.jwt) {
          const user: Usuario = { nombre: res.user.nombres, token: res.jwt };
          localStorage.setItem('usuario', JSON.stringify(user));
          this.userSubject.next(user);
          
        }
        return res;
      }
    ));
  }

  logout(){
    localStorage.clear();
    this.userSubject.next(null);
    this.router.navigateByUrl('/login')
  }


}
