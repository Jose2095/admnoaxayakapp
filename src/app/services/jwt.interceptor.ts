import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';


@Injectable()
export class JwtInterceptor implements HttpInterceptor{

    constructor(private _auth:AuthService){}

    intercept(req: HttpRequest<any>, next: HttpHandler): 
    Observable<HttpEvent<any>> {
        const user = this._auth.usuarioData;
        if (user){
            req= req.clone({
                setHeaders:{
                    Authorization:`Bearer ${user.token}`
                }
            });
        }
        return next.handle(req);
    }

}