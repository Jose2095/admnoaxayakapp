import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class VentasService {
  private url = 'https://oaxayak.mx/api/ventas/';

  constructor(private http: HttpClient) { }
  listadoVenta(): Observable<any> {
    return this.http.get(this.url+"get");
  }

  verDetalledeVenta(id: string): Observable<any> {
    return this.http.get(this.url +"data/"+ id);
  }
}
