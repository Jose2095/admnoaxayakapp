import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  private url = 'https://oaxayak.mx/api/categoria/';

  constructor(private http:HttpClient) { }

  getCategorias():Observable<any>{
    return this.http.get(this.url+"listar/cat");
  }

  guardarcategoria(titulo:string,descripcion:string){
    return this.http.post(this.url+"registrar",{titulo,descripcion});
  }

  eliminarCategoria(id:string){
    return this.http.delete(this.url+'/eliminar/'+id);
  }

  editarCategoria(id:string,titulo,descripcion){
   return this.http.put(this.url+"editar/"+id,{titulo,descripcion});
  }
}
