import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './services/auth.service';
import { Usuario } from './models/usuario';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})

export class AppComponent implements OnInit {
  public selectedIndex = 0;
  darkmode: boolean = true;
  public appPages = [
    {
      title: 'Inicio',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Ventas',
      url: '/ventas',
      icon: 'pricetag'
    },
    {
      title: 'Categorias',
      url: '/categorias',
      icon: 'apps'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public _authService: AuthService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      //this.statusBar.backgroundColorByHexString("ffffff")
      //this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    const prefersDark = JSON.parse(localStorage.getItem('dark'));
    if (prefersDark) {
      document.body.classList.toggle('dark');
      this.darkmode = true;
      this.statusBar.backgroundColorByHexString("#000000");
      this.statusBar.styleLightContent();
    } else {
      this.darkmode = false;
      this.statusBar.backgroundColorByHexString("ffffff")
      this.statusBar.styleDefault();
    }
  }

  cambiar() {
    this.darkmode = !this.darkmode;

    const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
    console.log(prefersDark);
    document.body.classList.toggle('dark');
    if (this.darkmode) {
      this.statusBar.backgroundColorByHexString("#000000");
      this.statusBar.styleLightContent();
    }
    else {
      this.statusBar.backgroundColorByHexString("ffffff")
      this.statusBar.styleDefault();
    }
    localStorage.setItem('dark', JSON.stringify(this.darkmode));


  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }

  }

}
